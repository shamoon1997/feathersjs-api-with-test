// See https://vincit.github.io/objection.js/#models
// for more of what you can do here.
const { Model } = require('objection')

class Books extends Model {
    static get tableName() {
        return 'books'
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['Title', 'Author', 'userid'],

            properties: {

                Title: 'string',
                Author: 'string',
                userid: 'integer',

            }
        }
    }


}

module.exports = function(app) {
    const db = app.get('knex')

    db.schema.hasTable('books').then(exists => {
            if (!exists) {
                db.schema.createTable('books', table => {
                        table.increments('id').primary();
                        table.string('Title')
                        table.string('Author')
                        table.integer('userid')
                        table.foreign('userid').references('users.id')
                    })
                    .then(() => console.log('Created books table')) // eslint-disable-line no-console
                    .catch(e => console.error('Error creating books table', e)) // eslint-disable-line no-console
            }
        })
        .catch(e => console.error('Error creating books table', e)) // eslint-disable-line no-console

    return Books
}