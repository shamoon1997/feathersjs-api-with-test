const { authenticate } = require('@feathersjs/authentication').hooks
const app = require('./books.service')

const {
    hashPassword,
    protect
} = require('@feathersjs/authentication-local').hooks

module.exports = {
    before: {
        all: [],
        find: [authenticate('jwt'),
            // function(context) {
            //     let id = context.params.user.id
            //     context.result = context.params.query.userid
            //     return context;
            // }
        ],
        get: [authenticate('jwt'),
            // function(context) {
            //     context.params.query.userid = context.params.user.id
            //     return context;
            // }

        ],
        create: [hashPassword('password')],
        update: [hashPassword('password'), authenticate('jwt'),
            function(context) {
                context.params.query.userid = context.params.user.id
                return context;
            }
        ],
        patch: [hashPassword('password'), authenticate('jwt'),
            function(context) {
                context.params.query.userid = context.params.user.id
                return context;
            }
        ],
        remove: [authenticate('jwt'),
            function(context) {
                context.params.query.userid = context.params.user.id
                return context;
            }
        ]
    },

    after: {
        all: [
            // Make sure the password field is never sent to the client
            // Always must be the last hook
            protect('password')
        ],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
}