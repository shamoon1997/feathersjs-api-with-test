const { Users } = require('./users.class');

const { authenticate } = require('@feathersjs/authentication').hooks

const {
    hashPassword,
    protect
} = require('@feathersjs/authentication-local').hooks

module.exports = {
    before: {
        all: [],
        find: [authenticate('jwt')
            // , function(context) {
            //     console.log(context.params);
            //     // context.query = context.params.user;
            //     // context.params.query.id = context.params.Users.id
            //     return context;
            // }
        ],
        get: [
            authenticate('jwt'),
            function(context) {
                context.result = context.params.user;
            }
        ],
        create: [hashPassword('password')],
        update: [hashPassword('password'), authenticate('jwt'),
            function(context) {
                console.log("id" + context.id);
                if (context.params.user.id != context.id) {
                    console.log('Error violating');
                    context.result = context.params.user;
                } else {
                    console.log('Good');
                }
                return context;
            }
        ],
        patch: [hashPassword('password'), authenticate('jwt'),
            function(context) {
                console.log("id" + context.id);
                if (context.params.user.id != context.id) {
                    console.log('Error violating');
                    context.result = context.params.user;
                } else {
                    console.log('Good');
                }
                return context;
            }
        ],
        remove: [authenticate('jwt'),
            function(context) {
                console.log("id" + context.id);
                if (context.params.user.id != context.id) {
                    console.log('Error violating');
                    context.result = context.params.user;
                } else {
                    console.log('Good');
                }
                return context;
            }
        ]
    },

    after: {
        all: [
            // Make sure the password field is never sent to the client
            // Always must be the last hook
            protect('password')
        ],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
}