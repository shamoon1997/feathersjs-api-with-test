const { access } = require('fs/promises')
const app = require('../src/app')


describe('authentication', () => {
    it('registered the authentication service', () => {
        expect(app.service('authentication')).toBeTruthy()
    })

    describe('local strategy', () => {
        const userInfo = {
            name: 'ZTBL',
            password: '9696'
        }

        beforeAll(async() => {
            try {
                await app.service('users').create(userInfo)
            } catch (error) {
                // Do nothing, it just means the user already exists and can be tested
            }
        })

        it('authenticates user and creates accessToken', async() => { //1
            const { user, accessToken } = await app.service('authentication').create({
                strategy: 'local',
                ...userInfo
            })
            expect(accessToken).toBeTruthy()
            expect(user).toBeTruthy()
        })

        it('See whether user is able to see others users', async() => { //2
            const user1 = await app.service('users').create({
                name: 'Robert',
                password: '123'
            })
            const user2 = await app.service('users').create({
                name: 'Chambadi',
                password: '123'
            })
            const user = await app.service('users').get(user2.id, {
                user: user1
            });
            expect(user.name).toBe(user1.name);
        })
        it('See whether user is able to update other users information', async() => { //3
            const user1 = await app.service('users').create({
                name: 'Andrew',
                password: '123'
            })
            const user2 = await app.service('users').create({
                name: 'Johonson',
                password: '123'
            })
            const updateduser = await app.service('users').update(user2.id, {
                name: "Butt",
                password: '321'
            }, {
                user: user1
            })
            expect(updateduser.id).toBe(user1.id);
        })
        it('see whether user is able to delete other users', async() => { //4
            const user1 = await app.service('users').create({
                name: 'Mike',
                password: '123'
            })
            const user2 = await app.service('users').create({
                name: 'Brand',
                password: '123'
            })
            const removeduser = await app.service('users').remove(user2.id, {
                user: user1
            })
            expect(removeduser.id).toBe(user1.id);
        })
        it('See whether user is able to update his own information', async() => { //5
            const user1 = await app.service('users').create({
                name: 'Zick',
                password: '123'
            })
            const updateduser = await app.service('users').update(user1.id, {
                name: 'Zick Butcher',
                password: '321'
            }, {
                user: user1
            })
            expect(updateduser.name).toBe('Zick Butcher')
        })
        it('See whether user is able to delete himself', async() => {
            const user1 = await app.service('users').create({
                name: 'Total',
                password: '123'
            })
            const deleteduser = await app.service('users').remove(user1.id, {
                user: user1
            })
            expect(deleteduser.name).toBe('Total')
        })
        it('Insert the book and check whether user can access that book', async() => { //4

            const user1 = await app.service('users').create({
                name: 'Kumar',
                password: '123'
            })
            const book1 = await app.service('books').create({
                Title: 'And that finally ends',
                Author: 'And that finally ends',
                userid: user1.id
            })
            const book = await app.service('books').get(book1.id, {
                user: user1
            });
            expect(book.userid).toBe(user1.id)
        })

        it('Insert the book and check whether another user can access book of other', async() => {
            const user1 = await app.service('users').create({
                name: 'Lenovo',
                password: '123'
            })
            const user2 = await app.service('users').create({
                name: 'Dell',
                password: '123'
            })
            const book1 = await app.service('books').create({
                Title: 'Intro to Nodejs',
                Author: 'Mosh Hamdeni',
                userid: user1.id
            })
            const book2 = await app.service('books').create({
                Title: 'Intro to Javascript',
                Author: 'Kyle',
                userid: user2.id
            })
            const book = await app.service('books').get(book1.id, {
                user: user2
            });
            expect(book.userid).toBe(user2.id);
        })
        it('Insert books and users and deletes other users books', async() => {
            const user1 = await app.service('users').create({
                name: 'Toyota',
                password: '123'
            })
            const user2 = await app.service('users').create({
                name: 'Honda',
                password: '123'
            })
            const book1 = await app.service('books').create({
                Title: 'Intro to Java',
                Author: 'Navin Reddey',
                userid: user1.id
            })
            const book2 = await app.service('books').create({
                Title: 'Intro to Database',
                Author: 'Sanchit Jain',
                userid: user2.id
            })

            const deletedbook = await app.service('books').remove(book2.id, {
                user: user1
            })
            expect(deletedbook.userid).toBe(user1.id)
        })
    })
})